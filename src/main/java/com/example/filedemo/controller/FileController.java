package com.example.filedemo.controller;
import com.mail.send.attachment.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@CrossOrigin("*")
@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    
    private static String UPLOADED_FOLDER = "D://temp//";
    @PostMapping("/sendingMail")
    public Boolean sendMailWithAttachment(@RequestParam("attachmentFile") MultipartFile attachmentFile, @RequestParam("mailReceipients") MultipartFile mailReceipient,
    		@RequestParam("mailContent") String mailContent, @RequestParam("subject") String subject ) {
    	
    	
    	 Path path = null;
         try {
        	 byte[] bytes = attachmentFile.getBytes();
        	 path = Paths.get(UPLOADED_FOLDER + attachmentFile.getOriginalFilename());
             System.out.println(UPLOADED_FOLDER + attachmentFile.getOriginalFilename());
             Files.write(path, bytes);
                   bytes = mailReceipient.getBytes();
                   path = Paths.get(UPLOADED_FOLDER + mailReceipient.getOriginalFilename());
                   Files.write(path, bytes);
                   File file = new File(UPLOADED_FOLDER + mailReceipient.getOriginalFilename()); 
                   Scanner sc = new Scanner(file); 
                   List<String[]> emails = new ArrayList<String[]>();
                   while (sc.hasNextLine()) {
                	   String[] values = sc.nextLine().split(",");
                	   emails.add(values);
                   }
             
        	 SendMail mail = new SendMail();
        	 for (String[]  receipient : emails) {
        		 for(String toMail : receipient) {
        			 mail.send(toMail.trim(), attachmentFile.getOriginalFilename(), UPLOADED_FOLDER + attachmentFile.getOriginalFilename(), mailContent, subject);
        		 }
        		 
			 }
         }catch(Exception e) {
         	logger.error("Exception in uploadFile",e);
         	e.printStackTrace();
         }
    	return true;
    }

   

}
