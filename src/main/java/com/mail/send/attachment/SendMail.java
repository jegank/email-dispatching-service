package com.mail.send.attachment;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMail {
	
	 private static String user;//change accordingly  
	 private static String password;//change accordingly  
	
	public boolean send(String toMail, String filename, String filePath, String mailContent, String subject) throws IOException {
		
		 FileReader reader=new FileReader("D://mail//mail-config.properties");  
	      
		    Properties p=new Properties();  
		    p.load(reader);  
		      
		   
		    user = p.getProperty("username");
		    password = p.getProperty("password");
		  
		
		 Properties properties = System.getProperties();  
		  properties.setProperty("mail.smtp.host", "smtp.gmail.com"); 
		  properties.put("mail.smtp.port", "587");
		  properties.put("mail.smtp.auth", "true"); //enable authentication
		  properties.put("mail.smtp.starttls.enable", "true");
		  Session session = Session.getDefaultInstance(properties,  
				   new javax.mail.Authenticator() {  
				   protected PasswordAuthentication getPasswordAuthentication() {  
				   return new PasswordAuthentication(user,password);  
				   }  
				  }); 
		  
		  //2) compose message     
		  try{  
		    MimeMessage message = new MimeMessage(session);  
		    message.setFrom(new InternetAddress(user));  
		    message.addRecipient(Message.RecipientType.TO,new InternetAddress(toMail));  
		    message.setSubject(subject);  
		      
		    //3) create MimeBodyPart object and set your message text     
		    BodyPart messageBodyPart1 = new MimeBodyPart();  
		    messageBodyPart1.setText(mailContent);  
		      
		    //4) create new MimeBodyPart object and set DataHandler object to this object      
		    MimeBodyPart messageBodyPart2 = new MimeBodyPart();  
		    DataSource source = new FileDataSource(filePath);  
		    messageBodyPart2.setDataHandler(new DataHandler(source));  
		    messageBodyPart2.setFileName(filename);  
		     
		     
		    //5) create Multipart object and add MimeBodyPart objects to this object      
		    Multipart multipart = new MimeMultipart();  
		    multipart.addBodyPart(messageBodyPart1);  
		    multipart.addBodyPart(messageBodyPart2);  
		    message.setContent(multipart ); 
		  
		    //6) set the multiplart object to the message object  
		    if(("true").equals(p.getProperty("mailSendflag"))) {
		    	 Transport.send(message);  
		    	 System.out.println("message  sent.to ..."+toMail);
		    }else {
		    	  System.out.println("message not sent...."+toMail);
		    }
		   
		   
		   
		   }catch (MessagingException ex) {
			   ex.printStackTrace();
			 }  
		  
		  
		return true;
	}

}
